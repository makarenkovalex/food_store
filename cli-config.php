<?php

use Symfony\Component\Dotenv\Dotenv;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// cli-config.php
require_once "vendor/autoload.php";

$dotenv = new Dotenv();
$dotenv->load('.env');

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(
    [__DIR__."/src/Entity"],
    $isDevMode
);
// database configuration parameters
$conn = array(
    'driver' => getenv('DB_DRIVER'),
    'host' => getenv('DB_HOST'),
    'dbname' => getenv('DB_NAME'),
    'user' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
);
// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
