<?php

namespace FoodStore;

/**
 * Просто базовый набор продуктов для тестирования
 */
class PreparedFoodsList
{
    public static function get()
    {
        return [
            'Milk',
            'Pepper',
            'Olive oil',
            'Salmon',
            'Potatoes',
            'Pasta',
            'Rice',
            'Chicken breasts',
        ];
    }
}