<?php

namespace FoodStore\Entity;

/**
 * Класс представляет промежуточную запись между заказом
 * и продуктом. Один заказ имеет много разных продуктов через эту
 * таблицу, один Payload связывает один заказ и один продукт.
 * Несколько Payload связывают разные продукты и их количество в один
 * заказ.
 * @Entity @Table(name="payloads")
 **/
class Payload
{
    /**
     * @Column(type="integer")
     */
    protected $quantity;
    /**
     * @Id
     * @ManyToOne(targetEntity="Order")
     * @JoinColumn(name="order_id", referencedColumnName="id")
     * @GeneratedValue(strategy="NONE")
     */
    protected $order;
    /**
     * @Id
     * @ManyToOne(targetEntity="Food")
     * @JoinColumn(name="food_id", referencedColumnName="id")
     * @GeneratedValue(strategy="NONE")
     */
    protected $food;

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function getFood()
    {
        return $this->food;
    }

    public function setFood($food)
    {
        $this->food = $food;
    }
}