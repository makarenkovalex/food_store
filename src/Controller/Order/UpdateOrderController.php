<?php

namespace FoodStore\Controller\Order;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use FoodStore\Entity\Order;
use FoodStore\Entity\Food;
use FoodStore\Entity\Payload;


/**
 * Обновляет статус заказа
 */
class UpdateOrderController
{
    /**
     * Основной метод контроллера
     * @param  Request       $request
     * @param  EntityManager $entityManager
     * @param  array         $parameters    Параметры из которых берется
     * orderId, передаваемый в uri
     * @return Response
     */
    public function hanlde(
        Request       $request,
        EntityManager $entityManager,
        array         $parameters
    ): Response
    {
        // TODO: add validation
        $jsonData = json_decode($request->getContent());
        $order = $entityManager->find(Order::class, $parameters['orderId']);
        $order->setStatus($jsonData->status);
        $entityManager->persist($order);
        $entityManager->flush();
        return new Response(
            json_encode(['orderId' => $order->getId()]),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }
}