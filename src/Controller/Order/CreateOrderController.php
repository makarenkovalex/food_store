<?php

namespace FoodStore\Controller\Order;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Date;
use FoodStore\Validator\Constraints\FoodExists;
use FoodStore\Entity\Order;
use FoodStore\Entity\Food;
use FoodStore\Entity\Payload;
use FoodStore\Error\ValidationError;
use DateTime;
use stdClass;
use Error;

class CreateOrderController
{
    protected $entityManager;

    /**
     * Основной метод контроллера
     * @param  Request       $request
     * @param  EntityManager $entityManager
     * @param  array         $parameters    Параметры из которых берется
     * orderId, передаваемый в uri
     * @return Response
     */
    public function hanlde(
        Request       $request,
        EntityManager $entityManager,
        array         $parameters
    ): Response
    {
        $this->entityManager = $entityManager;
        $jsonData = $this->validate($request->getContent());
        // создать заказ
        $order = new Order;
        $order->setStatus(Order::STATUS_NEW);
        $order->setDeliveryAddress($jsonData->delivery_address);
        $order->setDeliveryDate(new DateTime($jsonData->delivery_date));
        $foods = [];
        $payloads = [];
        // создать содержимое заказа для каждой позиции
        foreach ($jsonData->foods as $foodData) {
            $food = $entityManager->find(Food::class, $foodData->id);
            $payload = new Payload;
            $payload->setQuantity($foodData->quantity);
            $payload->setOrder($order);
            $payload->setFood($food);
            $payloads[] = $payload;
            $order->addPayload($payload);
            $food->addPayload($payload);
        }
        // сохранить
        $entityManager->persist($food);
        $entityManager->persist($order);
        foreach ($payloads as $payload) {
            $entityManager->persist($payload);
        }
        $entityManager->flush();
        return new Response(
            json_encode(['orderId' => $order->getId()]),
            Response::HTTP_CREATED,
            ['content-type' => 'application/json']
        );
    }

    /**
     * Делает проверки содержимого json из запроса
     * @param  string $content json строка из запроса
     * @throws ValidationError      если есть ошибки валидации
     * @return stdClass             объект из декодированной json строки
     */
    protected function validate(string $content): stdClass
    {
        // проверка на ошибки синтаксиса json
        $jsonData = json_decode($content);
        if (JSON_ERROR_NONE  !== json_last_error()) {
            throw new ValidationError("json is malformed");
        }
        // проверить что foods существует
        if (isset($jsonData->foods)) {
            // что foods не пустой
            if (!empty($jsonData->foods)) {
                $hasViolations = false;
                $errors = '';
                $uniqueFoodIds = [];
                // проверить каждую позицию 
                foreach ($jsonData->foods as $food) {
                    if (isset($food->id) && isset($food->quantity)) {
                        // проверка что бы два раза не посылали один и тот же
                        // продукт
                        if (array_key_exists((int)$food->id, $uniqueFoodIds)) {
                            throw new ValidationError("food id {$food->id} is not unique");
                        } else {
                            $uniqueFoodIds[(int)$food->id] = true;
                        }
                        // проверить поля продукта
                        $this->validateFood($food, $hasViolations, $errors);
                    } else {
                        throw new ValidationError("missing food id or food quantity");
                    }
                }
                // проверка полей адреса и даты доставки
                $this->validateDelivery(
                    $jsonData->delivery_address,
                    $jsonData->delivery_date,
                    $hasViolations,
                    $errors
                );
                if ($hasViolations) {
                    throw new ValidationError("$errors");
                } else {
                    return $jsonData;
                }
            } else {
                throw new ValidationError("foods array is empty");
            }
        } else {
            throw new ValidationError("missing foods array");
        }
    }

    /**
     * Проверка одной позиции продукта из json
     * @param  stdClass $food           объект содержащий поля id и quantity
     * @param  boolean  &$hasViolations ссылка на boolean признак наличия ошибок
     * @param  string   &$errors        ссылка на строку с ошибками
     */
    protected function validateFood(stdClass $food, bool &$hasViolations, string &$errors): void
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($food->id, [
            new NotBlank(),
            new FoodExists([], $this->entityManager),
        ]);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors .= $violation . ' ';
            }
            $hasViolations = true;
        }
        $violations = $validator->validate((int)$food->quantity, [
            new Range(['min' => '1', 'max' => '999999'])
        ]);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors .= $violation . '; ';
            }
            $hasViolations = true;
        }
    }

    /**
     * Проверяет параметры доставки (дату, адрес)
     * @param  string  $delivery_address
     * @param  string  $delivery_date
     * @param  bool    &$hasViolations
     * @param  string  &$errors
     */
    protected function validateDelivery(
        string $delivery_address,
        string $delivery_date,
        bool   &$hasViolations,
        string &$errors
    )
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($delivery_address, [
            new NotBlank(),
            new Length(['max' => '255']),
        ]);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors .= $violation . ' ';
            }
            $hasViolations = true;
        }
        $violations = $validator->validate($delivery_date, [
            new NotBlank(),
            new Date()
        ]);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors .= $violation . '; ';
            }
            $hasViolations = true;
        }
    }
}