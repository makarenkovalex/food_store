<?php

namespace FoodStore\Controller\Order;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\NotBlank;
use FoodStore\Validator\Constraints\OrderExists;
use Doctrine\ORM\EntityManager;
use FoodStore\Entity\Order;
use FoodStore\Entity\Food;
use FoodStore\Entity\Payload;
use FoodStore\Error\ValidationError;

class ShowOrderController
{
    /**
     * Основной метод контроллера
     * @param  Request       $request
     * @param  EntityManager $entityManager
     * @param  array         $parameters    Параметры из uri и прочее
     * @return Response
     */
    public function hanlde(
        Request       $request,
        EntityManager $entityManager,
        array         $parameters
    ): Response
    {
        $this->entityManager = $entityManager;
        $this->validate($parameters['orderId']);
        $query = $entityManager->createQuery('
            SELECT o, p, f 
            FROM FoodStore\Entity\Order o 
            JOIN o.payloads p 
            JOIN p.food f
            WHERE o.id = :orderId'
        );
        $query->setParameter('orderId', $parameters['orderId']);
        $order = $query->getSingleResult();
        $data = [];
        foreach ($order->getPayloads() as $payload) {
            $data[] = [
                'food_name' => $payload->getFood()->getName(),
                'quantity' => $payload->getQuantity(),
            ];
        }
        return new Response(
            json_encode($data),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }

    /**
     * Проверка что такой заказ существует
     * @param  string $orderId 
     * @throws ValidationError
     */
    protected function validate(string $orderId)
    {
        $hasViolations = false;
        $errors = '';
        $validator = Validation::createValidator();
        $violations = $validator->validate($orderId, [
            new NotBlank(),
            new OrderExists([], $this->entityManager),
        ]);
        if (0 !== count($violations)) {
            foreach ($violations as $violation) {
                $errors .= $violation . ' ';
            }
            $hasViolations = true;
        }
        if ($hasViolations) {
            throw new ValidationError("$errors");
        }
    }
}